'use strict';

module.exports = function (sequelize, DataTypes) {
    const Snapshot = sequelize.define('Snapshot',
        {
            name: {
                type: DataTypes.STRING(64),
                allowNull: false
            },
            code: {
                type: DataTypes.TEXT,
                allowNull: false
            }
        }, {
            classMethods: {
                timestamps: true,
                createdAt: 'createdAt',
                updatedAt: 'updatedAt',
                associate: (models) => {
                    Snapshot.belongsTo(models.User, {foreignKey: 'user_id', foreignKeyConstraint: true});
                }
            }
        }
    );

    return Snapshot;
};
