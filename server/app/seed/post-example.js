'use strict';

module.exports = [{
    title: 'ECMAScript 6 — New Features',
    alias: 'ES6-features',
    text: '2nd paragraph. *Italic*, **bold**, and `monospace`. Itemized lists look like: '
    + '* this one * that one '
    + '* the other oneHello.\n\n* This is markdown.\n* It is fun\n* Love it or leave it.\n'
    + '\n\nContrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece'
    + ' of classical Latin literature from 45 BC,'
    + ' making it over 2000 years old. Richard McClintock, a Latin professor at Hampden-Sydney '
    + 'College in Virginia, looked up one of the more obscure Latin words, consectetur, from a Lorem '
    + 'Ipsum passage, and going through the cites of the word in classical literature, discovered the '
    + 'undoubtable source. Lorem Ipsum comes from sections 1.10.32 and 1.10.33 of "de Finibus Bonorum et '
    + 'Malorum" (The Extremes of Good and Evil) by Cicero, written in 45 BC. This book is a treatise on the theory '
    + 'of ethics, very popular during the Renaissance. The first line of Lorem Ipsum, "Lorem ipsum dolor sit'
    + ' amet..", comes from a line in section 1.10.32.',
    user_id: 1
}, {
    title: 'new',
    alias: 'new',
    text: '2nd paragraph. *Italic*, **bold**, and `monospace`. Itemized lists look like: '
    + '* this one * that one '
    + '* the other oneHello.\n\n* This is markdown.\n* It is fun\n* Love it or leave it.\n'
    + '\n\nContrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece'
    + ' of classical Latin literature from 45 BC,'
    + ' making it over 2000 years old. Richard McClintock, a Latin professor at Hampden-Sydney '
    + 'College in Virginia, looked up one of the more obscure Latin words, consectetur, from a Lorem '
    + 'Ipsum passage, and going through the cites of the word in classical literature, discovered the '
    + 'undoubtable source. Lorem Ipsum comes from sections 1.10.32 and 1.10.33 of "de Finibus Bonorum et '
    + 'Malorum" (The Extremes of Good and Evil) by Cicero, written in 45 BC. This book is a treatise on the theory '
    + 'of ethics, very popular during the Renaissance. The first line of Lorem Ipsum, "Lorem ipsum dolor sit'
    + ' amet..", comes from a line in section 1.10.32.',
    user_id: 1
}, {
    title: 'new1',
    alias: 'new1',
    text: '2nd paragraph. *Italic*, **bold**, and `monospace`. Itemized lists look like: '
    + '* this one * that one '
    + '* the other oneHello.\n\n* This is markdown.\n* It is fun\n* Love it or leave it.\n'
    + '\n\nContrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece'
    + ' of classical Latin literature from 45 BC,'
    + ' making it over 2000 years old. Richard McClintock, a Latin professor at Hampden-Sydney '
    + 'College in Virginia, looked up one of the more obscure Latin words, consectetur, from a Lorem '
    + 'Ipsum passage, and going through the cites of the word in classical literature, discovered the '
    + 'undoubtable source. Lorem Ipsum comes from sections 1.10.32 and 1.10.33 of "de Finibus Bonorum et '
    + 'Malorum" (The Extremes of Good and Evil) by Cicero, written in 45 BC. This book is a treatise on the theory '
    + 'of ethics, very popular during the Renaissance. The first line of Lorem Ipsum, "Lorem ipsum dolor sit'
    + ' amet..", comes from a line in section 1.10.32.',
    user_id: 1
}, {
    title: 'new2',
    alias: 'new2',
    text: '2nd paragraph. *Italic*, **bold**, and `monospace`. Itemized lists look like: '
    + '* this one * that one '
    + '* the other oneHello.\n\n* This is markdown.\n* It is fun\n* Love it or leave it.\n'
    + '\n\nContrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece'
    + ' of classical Latin literature from 45 BC,'
    + ' making it over 2000 years old. Richard McClintock, a Latin professor at Hampden-Sydney '
    + 'College in Virginia, looked up one of the more obscure Latin words, consectetur, from a Lorem '
    + 'Ipsum passage, and going through the cites of the word in classical literature, discovered the '
    + 'undoubtable source. Lorem Ipsum comes from sections 1.10.32 and 1.10.33 of "de Finibus Bonorum et '
    + 'Malorum" (The Extremes of Good and Evil) by Cicero, written in 45 BC. This book is a treatise on the theory '
    + 'of ethics, very popular during the Renaissance. The first line of Lorem Ipsum, "Lorem ipsum dolor sit'
    + ' amet..", comes from a line in section 1.10.32.',
    user_id: 1
}, {
    title: 'new3',
    alias: 'new3',
    text: '2nd paragraph. *Italic*, **bold**, and `monospace`. Itemized lists look like: '
    + '* this one * that one '
    + '* the other oneHello.\n\n* This is markdown.\n* It is fun\n* Love it or leave it.\n'
    + '\n\nContrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece'
    + ' of classical Latin literature from 45 BC,'
    + ' making it over 2000 years old. Richard McClintock, a Latin professor at Hampden-Sydney '
    + 'College in Virginia, looked up one of the more obscure Latin words, consectetur, from a Lorem '
    + 'Ipsum passage, and going through the cites of the word in classical literature, discovered the '
    + 'undoubtable source. Lorem Ipsum comes from sections 1.10.32 and 1.10.33 of "de Finibus Bonorum et '
    + 'Malorum" (The Extremes of Good and Evil) by Cicero, written in 45 BC. This book is a treatise on the theory '
    + 'of ethics, very popular during the Renaissance. The first line of Lorem Ipsum, "Lorem ipsum dolor sit'
    + ' amet..", comes from a line in section 1.10.32.',
    user_id: 1
}, {
    title: 'new4',
    alias: 'new4',
    text: '2nd paragraph. *Italic*, **bold**, and `monospace`. Itemized lists look like: '
    + '* this one * that one '
    + '* the other oneHello.\n\n* This is markdown.\n* It is fun\n* Love it or leave it.\n'
    + '\n\nContrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece'
    + ' of classical Latin literature from 45 BC,'
    + ' making it over 2000 years old. Richard McClintock, a Latin professor at Hampden-Sydney '
    + 'College in Virginia, looked up one of the more obscure Latin words, consectetur, from a Lorem '
    + 'Ipsum passage, and going through the cites of the word in classical literature, discovered the '
    + 'undoubtable source. Lorem Ipsum comes from sections 1.10.32 and 1.10.33 of "de Finibus Bonorum et '
    + 'Malorum" (The Extremes of Good and Evil) by Cicero, written in 45 BC. This book is a treatise on the theory '
    + 'of ethics, very popular during the Renaissance. The first line of Lorem Ipsum, "Lorem ipsum dolor sit'
    + ' amet..", comes from a line in section 1.10.32.',
    user_id: 1
}, {
    title: 'new5',
    alias: 'new5',
    text: '2nd paragraph. *Italic*, **bold**, and `monospace`. Itemized lists look like: '
    + '* this one * that one '
    + '* the other oneHello.\n\n* This is markdown.\n* It is fun\n* Love it or leave it.\n'
    + '\n\nContrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece'
    + ' of classical Latin literature from 45 BC,'
    + ' making it over 2000 years old. Richard McClintock, a Latin professor at Hampden-Sydney '
    + 'College in Virginia, looked up one of the more obscure Latin words, consectetur, from a Lorem '
    + 'Ipsum passage, and going through the cites of the word in classical literature, discovered the '
    + 'undoubtable source. Lorem Ipsum comes from sections 1.10.32 and 1.10.33 of "de Finibus Bonorum et '
    + 'Malorum" (The Extremes of Good and Evil) by Cicero, written in 45 BC. This book is a treatise on the theory '
    + 'of ethics, very popular during the Renaissance. The first line of Lorem Ipsum, "Lorem ipsum dolor sit'
    + ' amet..", comes from a line in section 1.10.32.',
    user_id: 1
}, {
    title: 'new6',
    alias: 'new6',
    text: '2nd paragraph. *Italic*, **bold**, and `monospace`. Itemized lists look like: '
    + '* this one * that one '
    + '* the other oneHello.\n\n* This is markdown.\n* It is fun\n* Love it or leave it.\n'
    + '\n\nContrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece'
    + ' of classical Latin literature from 45 BC,'
    + ' making it over 2000 years old. Richard McClintock, a Latin professor at Hampden-Sydney '
    + 'College in Virginia, looked up one of the more obscure Latin words, consectetur, from a Lorem '
    + 'Ipsum passage, and going through the cites of the word in classical literature, discovered the '
    + 'undoubtable source. Lorem Ipsum comes from sections 1.10.32 and 1.10.33 of "de Finibus Bonorum et '
    + 'Malorum" (The Extremes of Good and Evil) by Cicero, written in 45 BC. This book is a treatise on the theory '
    + 'of ethics, very popular during the Renaissance. The first line of Lorem Ipsum, "Lorem ipsum dolor sit'
    + ' amet..", comes from a line in section 1.10.32.',
    user_id: 1
}];
