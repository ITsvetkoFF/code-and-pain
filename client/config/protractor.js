'use strict';

const fs = require('fs');

let protractorBase = `${__dirname}/../node_modules/protractor/`;

if (!fs.existsSync(`${protractorBase}/selenium`)) {
    protractorBase = `${__dirname}/../node_modules/gulp-protractor/node_modules/protractor/`;
}

const webdriverVersions = require(`${protractorBase}config.json`).webdriverVersions;

const capabilities = [
    {
        browserName: 'chrome',
        chromeOptions: {
            args: ['no-sandbox']
        }
    }
];

if (process.platform === 'win32') {
    capabilities.push({
        browserName: 'internet explorer',
        platform: 'ANY',
        version: '11'
    });
} else if (process.platform === 'darwin') {
    capabilities.push({
        browserName: 'safari'
    });
}

module.exports.config = {
    multiCapabilities: capabilities,
    seleniumServerJar: `${protractorBase}selenium/selenium-server-standalone-${webdriverVersions.selenium}.jar`,
    baseUrl: 'http://localhost:8088/index.html#',
    framework: 'mocha',
    specs: ['../test/e2e/*-test.js'],
    maxSessions: 1,
    rootElement: '#applicationContainer',
    mochaOpts: {
        timeout: 36000
    }
};

