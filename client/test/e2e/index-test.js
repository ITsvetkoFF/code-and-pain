const chai = require('chai');
const promised = require('chai-as-promised');
chai.use(promised);
const expect = chai.expect;


describe('Index State', () => {
    beforeEach(() => {
        browser.ignoreSynchronization = true;
        browser.get('http://localhost:8088/#/index');
        browser.sleep(500);
    });

    it('should verify site has title', () => {
        expect(browser.getTitle()).to.eventually.equal('Pain and code');
    });

    it('should verify there are header with text LOGIN on the page', () => {
        browser.wait(element(by.css('h3')).isDisplayed, 1000);
        expect(browser.findElement(By.css('h3')).getText()).to.eventually.equal('LOGIN');
    });
});
