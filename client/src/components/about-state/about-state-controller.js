class AboutStateController {
    constructor() {
        this.confirmMesssageShow = false;
        this.user = '';
    }

    showConfirmMessage() {
        this.confirmMesssageShow = true;
    }
}

export default [
    AboutStateController
];
