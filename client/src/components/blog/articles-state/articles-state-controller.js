class ArticlesStateController {
    /**
     * @param {ArticlesService} ArticlesService - Service provides retrieving articles from the server
     * @param {TagsService} TagsService - Service provides retrieving articles from the server
     */
    constructor(ArticlesService, TagsService, $stateParams, $state, $scope) {
        this.ArticlesService = ArticlesService;
        this.TagsService = TagsService;
        this.$stateParams = $stateParams;
        this.$state = $state;
        this.$scope = $scope;
        this.displayAllTagName = 'Show everything';
        this.activeTag = $stateParams.tag;
        if (this.activeTag === undefined) {
            this.$state.go('app.blog.articles', {tag: this.displayAllTagName});
        }
        this.fetchArticles(this.activeTag);
        this.TagsService.getTags();
    }

    /**
     * Populate list of articles in ArticlesService.
     * If the tag is not specified - fetch articles with any tags.
     *
     * @return {array} - Array of articles stored in the ArticlesService
     */
    fetchArticles(tag) {
        if (tag === this.displayAllTagName) {
            this.ArticlesService.getArticles().then((data) => {
                this.$scope.$broadcast('loadedArticles', data);
                return data;
            });
        } else {
            this.ArticlesService.getArticlesByTag(tag).then((data) => {
                this.$scope.$broadcast('loadedArticles', data);
                return data;
            });
        }
    }

    redirectToTag(tag) {
        this.$state.go('app.blog.articles', {tag});
    }

    showProfile(id) {
        this.$state.go('app.profile', {id});
    }
}

export default [
    'ArticlesService',
    'TagsService',
    '$stateParams',
    '$state',
    '$scope',
    ArticlesStateController
];
