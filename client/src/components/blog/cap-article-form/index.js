import angular from 'angular';
import './cap-article-form-directive.css!';
import CapArticleFormDirectiveController from './cap-article-form-directive-controller';
import capArticleFormDirective from './cap-article-form-directive';
import capTextEditorComponent from './../cap-text-editor/index';
import capTagList from './../cap-tag-list/index';

import 'components/blog/cap-text-editor/index';

const dependencies = [
    capTextEditorComponent.name,
    capTagList
];

export default angular
    .module('cap-article-form-directive-component', dependencies)
    .controller('CapArticleFormDirectiveController', CapArticleFormDirectiveController)
    .directive('capArticleForm', capArticleFormDirective);
