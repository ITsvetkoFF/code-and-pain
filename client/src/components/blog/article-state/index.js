import './article-state.css!';
import angular from 'angular';
import 'angular-ui-router';
import ArticleStateController from './article-state-controller';
import articleRouteConfig from './article-route';
import capArticleComponent from './../cap-article/index';

const dependencies = [
    'ui.router',
    capArticleComponent.name
];

export default angular
    .module('article-state-component', dependencies)
    .controller('ArticleStateController', ArticleStateController)
    .config(articleRouteConfig);
