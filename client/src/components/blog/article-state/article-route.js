import template from './article-state.html!text';

function articleRouteConfig($stateProvider) {
    $stateProvider
        .state('app.blog.article', {
            url: '/articles/:alias',
            views: {
                blog: {
                    controller: 'ArticleStateController as articleState',
                    template
                }
            }
        });
}

export default [
    '$stateProvider',
    articleRouteConfig
];
