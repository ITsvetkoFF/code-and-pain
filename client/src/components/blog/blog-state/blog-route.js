import template from './blog-state.html!text';

function blogRouteConfig($stateProvider) {
    $stateProvider
        .state('app.blog', {
            url: 'blog',
            redirectTo: 'app.blog.articles',
            views: {
                application: {
                    controller: 'BlogStateController as blogState',
                    template
                }
            }
        });
}

export default [
    '$stateProvider',
    blogRouteConfig
];
