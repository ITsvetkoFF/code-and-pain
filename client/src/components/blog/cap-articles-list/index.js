import './cap-articles-list-component.css!';
import angular from 'angular';
import CapArticlesListComponentController from './cap-articles-list-component-controller';
import capArticlesListComponent from './cap-articles-list-component';
import 'angular-utils-pagination';

const dependencies = [
    'angularUtils.directives.dirPagination'
];

export default angular
    .module('cap-articles-list-component', dependencies)
    .controller('CapArticlesListComponentController', CapArticlesListComponentController)
    .component('capArticlesList', capArticlesListComponent);
