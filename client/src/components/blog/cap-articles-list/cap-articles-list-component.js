import template from './cap-articles-list-component.html!text';

export default {
    bindings: {
        articles: '<',
        showProfile: '&'
    },
    bindToController: true,
    controller: 'CapArticlesListComponentController',
    require: { },
    template
};
