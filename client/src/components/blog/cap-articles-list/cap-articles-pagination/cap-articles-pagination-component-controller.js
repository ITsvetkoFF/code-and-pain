class CapArticlesPaginationComponentController {
    constructor($scope) {
        this.pageNumbers = [];
        this.$scope = $scope;
        $scope.$on('separatedArticles', (event, args) => {
            this.generatePageNumbers(args);
        });
    }

    generatePageNumbers(count) {
        for (let i = 0; i < count; i++) {
            this.pageNumbers.push(i + 1);
        }
    }
}

export default [
    '$scope',
    CapArticlesPaginationComponentController
];
