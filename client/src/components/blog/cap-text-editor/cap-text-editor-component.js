import template from './cap-text-editor-component.html!text';

export default {
    bindings: {
        text: '='
    },
    bindToController: true,
    controller: 'CapTextEditorComponentController',
    require: { },
    template
};
