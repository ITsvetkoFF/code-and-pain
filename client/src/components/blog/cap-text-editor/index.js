import './cap-text-editor-component.css!';
import angular from 'angular';
import CapTextEditorComponentController from './cap-text-editor-component-controller';
import capTextEditorComponent from './cap-text-editor-component';
import capTextEditorDirective from './cap-text-editor-directive';

import 'codemirror/lib/codemirror.css!';
import 'codemirror/mode/javascript/javascript.js';
import 'codemirror/addon/hint/javascript-hint.js';
import 'codemirror/addon/hint/show-hint.js';
import 'codemirror/addon/hint/show-hint.css!';
import 'codemirror/addon/dialog/dialog.js';
import 'codemirror/addon/dialog/dialog.css!';
import 'codemirror/addon/search/search.js';
import 'codemirror/addon/search/searchcursor.js';
import 'codemirror/addon/display/fullscreen.js';
import 'codemirror/addon/display/fullscreen.css!';
import 'codemirror/mode/markdown/markdown.js';

const dependencies = [
];

export default angular
    .module('cap-text-editor-component', dependencies)
    .controller('CapTextEditorComponentController', CapTextEditorComponentController)
    .component('capTextEditor', capTextEditorComponent)
    .directive('capBlogTextEditor', capTextEditorDirective);

