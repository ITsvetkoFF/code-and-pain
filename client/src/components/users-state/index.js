import './users-state.css!';
import angular from 'angular';
import 'angular-ui-router';
import UsersStateController from './users-state-controller';
import usersRouteConfig from './users-route';
import capUsersListComponent from './cap-users-list/index';

const dependencies = [
    'ui.router',
    capUsersListComponent.name
];

export default angular
    .module('users-state-component', dependencies)
    .controller('UsersStateController', UsersStateController)
    .config(usersRouteConfig);
