class UsersStateController {
    constructor(userService, logger, $state) {
        this.userService = userService;
        this.logger = logger;
        this.$state = $state;
        this.users = [];
        this.initUsersList();
    }

    /**
     * Retrieves users array from the server and assigns it into the initialUsers property. If users array is retrieved,
     * initial sorting is made.
     */
    initUsersList() {
        this.userService.getAll()
            .then((users) => {
                this.users = users;
            })
            .catch(() => {
                this.$state.go('app.code-editor');
            });
    }

    /**
     * Sends user object to the server for update
     *
     * @param {Object} user - An user object
     */
    updateUser(user) {
        this.userService.update(user.id, user)
            .then(() => {
                this.logger.success('User\'s profile successfully updated', 'Success');
            })
            .catch((error) => {
                this.logger.error(error, 'Error');
            });
    }

}

export default [
    'userService',
    'logger',
    '$state',
    UsersStateController
];
