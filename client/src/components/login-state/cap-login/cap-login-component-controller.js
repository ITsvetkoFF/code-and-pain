class CapLoginComponentController {
    constructor(authService, $state, API_URL) {
        this.authService = authService;
        this.$state = $state;
        this.API_URL = API_URL;
    }

    submitLogin() {
        this.authService.login(this.fields)
            .then(() => {
                if (this.authService.isLoggedIn()) {
                    this.$state.go('app.code-editor');
                }
            });
    }
}
export default [
    'authService',
    '$state',
    'API_URL',
    CapLoginComponentController
];
