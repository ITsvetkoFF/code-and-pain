import futureStates from './states.json!';

function routingConfig($locationProvider, $urlRouterProvider, $httpProvider,
                       $futureStateProvider, $urlMatcherFactoryProvider) {
    $urlMatcherFactoryProvider.caseInsensitive(true);
    $urlMatcherFactoryProvider.strictMode(false);
    futureStates.forEach((state) => $futureStateProvider.futureState(state));
    $httpProvider.useApplyAsync(true);
    $locationProvider.html5Mode(false);
    $urlRouterProvider.otherwise('/code-editor');
    $urlRouterProvider.when('/', '/code-editor');
}

export default [
    '$locationProvider',
    '$urlRouterProvider',
    '$httpProvider',
    '$futureStateProvider',
    '$urlMatcherFactoryProvider',
    routingConfig
];
