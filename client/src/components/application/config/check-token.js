function checkTokenAndLogin($window, $http) {
    // TODO: validate token each time this code runs. We need some BE route for that
    const user = JSON.parse($window.localStorage.getItem('user'));
    if (user) {
        $http.defaults.headers.common.Authorization = `Bearer ${user.token}`;
    }
}

export default [
    '$window',
    '$http',
    checkTokenAndLogin
];
