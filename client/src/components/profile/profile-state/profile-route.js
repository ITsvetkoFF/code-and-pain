import template from './profile-state.html!text';

function profileRouteConfig($stateProvider) {
    $stateProvider
        .state('app.profile', {
            url: 'profile/{id:int}',
            views: {
                application: {
                    controller: 'ProfileStateController as profileState',
                    template
                }
            }
        });
}

export default [
    '$stateProvider',
    profileRouteConfig
];
