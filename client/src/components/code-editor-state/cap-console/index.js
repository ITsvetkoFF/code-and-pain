import './cap-console-component.css!';
import angular from 'angular';
import CapConsoleComponentController from './cap-console-component-controller';
import capConsoleComponent from './cap-console-component';

const dependencies = [
];

export default angular
    .module('cap-console-component', dependencies)
    .controller('CapConsoleComponentController', CapConsoleComponentController)
    .component('capConsole', capConsoleComponent);
