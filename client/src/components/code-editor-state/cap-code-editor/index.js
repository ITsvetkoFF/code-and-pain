import './cap-code-editor-component.css!';
import angular from 'angular';
import CapCodeEditorComponentController from './cap-code-editor-component-controller';
import capCodeEditorComponent from './cap-code-editor-component';
import capCodeEditorDirective from './cap-code-editor-directive';

import 'codemirror/lib/codemirror.css!';
import 'codemirror/mode/javascript/javascript.js';
import 'codemirror/addon/hint/javascript-hint.js';
import 'codemirror/addon/hint/show-hint.js';
import 'codemirror/addon/hint/show-hint.css!';
import 'codemirror/addon/dialog/dialog.js';
import 'codemirror/addon/dialog/dialog.css!';
import 'codemirror/addon/search/search.js';
import 'codemirror/addon/search/searchcursor.js';
import 'codemirror/addon/display/fullscreen.js';
import 'codemirror/addon/display/fullscreen.css!';
import 'codemirror/mode/markdown/markdown.js';

const dependencies = [
];

export default angular
    .module('cap-code-editor-component', dependencies)
    .controller('CapCodeEditorComponentController', CapCodeEditorComponentController)
    .component('capCodeEditor', capCodeEditorComponent)
    .directive('capCodeAndPainEditor', capCodeEditorDirective);
