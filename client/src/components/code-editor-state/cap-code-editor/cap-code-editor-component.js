import template from './cap-code-editor-component.html!text';

export default {
    bindings: {
        inputCode: '=',
        onSetInputCode: '&',
        onCompileCode: '&'
    },
    bindToController: true,
    controller: 'CapCodeEditorComponentController',
    require: {},
    template
};
